<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('verified');

Route::namespace('App\Http\Controllers\Admin')->prefix('admin')->middleware(['auth','auth.admin','verified'])->name('admin.')->group(function(){
    Route::resource('/users','UserController',['except'=>['show','create','store']]);
    Route::resource('/cvmenagement','CvMenagementController',['except'=>['destroy','create','store','edit','update','create','store','show']]);
    Route::get('/cvblade/{id}','CvMenagementController@cvblade')->name('cvmenagement.cvblade');

});

Route::namespace('App\Http\Controllers\CV')->middleware('auth','verified')->group(function(){
    Route::get('/index', 'CvController@index')->name('cv.index');
    Route::get('/create' ,'CvController@create')->name('cv.create');
    Route::post('/create', 'CvController@store')->name("cv.store");
    Route::get('/edit', 'CvController@edit')->name("cv.edit");
    Route::delete('/edit','CvController@destroy')->name('cv.destroy');
    Route::post('/edit/', 'CvController@update')->name("cv.update");
    Route::post('/edit/', 'CvController@update')->name("cv.update");


});
