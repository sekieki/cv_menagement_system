@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <label class="float-left mt-2">{{ __('Your CV') }}</label>
                        @if($contacts->isEmpty()&&$academics->isEmpty()&&$professionals->isEmpty()&&$qualifications->isEmpty()&&$awards->isEmpty()&&$grants->isEmpty()&&$licenses->isEmpty())
                        <ul class="navbar-nav float-right">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('cv.create')}}">
                                    {{ __('Create your CV') }}
                                </a>
                            </li>
                        </ul>
                        @else
                        <ul class="navbar-nav float-right mr-2">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('cv.edit')}}">
                                    {{ __('Edit your CV') }}
                                </a>
                            </li>
                        </ul>
                            @endif
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($contacts->isEmpty()&&$academics->isEmpty()&&$professionals->isEmpty()&&$qualifications->isEmpty()&&$awards->isEmpty()&&$grants->isEmpty()&&$licenses->isEmpty())
                                <label>CV not created yet</label>
                            @else
                                <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Contact Information</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-3">Name</th>
                                <th class="col-sm-3">Email</th>
                                <th class="col-sm-3">Number</th>
                                <th class="col-sm-3">Address</th>
                            </tr>
                            @foreach($contacts as $contact)
                                <tr class="d-flex">
                                    <td class="col-sm-3">{{$contact->name}}</td>
                                    <td class="col-sm-3">{{$contact->email}}</td>
                                    <td class="col-sm-3">{{$contact->telefon_number}}</td>
                                    <td class="col-sm-3">{{$contact->address}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Academic history</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-3">School Name</th>
                                <th class="col-sm-3">Title of the degree</th>
                                <th class="col-sm-3">The year you started</th>
                                <th class="col-sm-3">The year you graduated</th>
                            </tr>
                            @foreach($academics as $academic)
                                <tr class="d-flex">
                                    <td class="col-sm-3">{{$academic->school}}</td>
                                    <td class="col-sm-3">{{$academic->title_of_degree}}</td>
                                    <td class="col-sm-3">{{$academic->year_started}}</td>
                                    <td class="col-sm-3">{{$academic->year_graduated}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Professional experience</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-3">Organization name</th>
                                <th class="col-sm-3">Job title</th>
                                <th class="col-sm-3">The date you were employed</th>
                                <th class="col-sm-3">Experience and achievements</th>
                            </tr>
                            @foreach($professionals as $professional)
                                <tr class="d-flex">
                                    <td class="col-sm-3">{{$professional->organization_name}}</td>
                                    <td class="col-sm-3">{{$professional->job_title}}</td>
                                    <td class="col-sm-3">{{$professional->date_employed}}</td>
                                    <td class="col-sm-3">{{$professional->experience_achievements}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Qualifications and skills</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-6">Hard skills</th>
                                <th class="col-sm-6">Soft skills</th>
                            </tr>
                            @foreach($qualifications as $qualification)
                                <tr class="d-flex">
                                    <td class="col-sm-6">{{$qualification->hard_skills}}</td>
                                    <td class="col-sm-6">{{$qualification->soft_skills}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Awards and honors</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-3">Award name</th>
                                <th class="col-sm-3">Year received</th>
                                <th class="col-sm-3">Organization name</th>
                                <th class="col-sm-3">Pertinent details</th>
                            </tr>
                            @foreach($awards as $award)
                                <tr class="d-flex">
                                    <td class="col-sm-3">{{$award->award_name}}</td>
                                    <td class="col-sm-3">{{$award->year_received}}</td>
                                    <td class="col-sm-3">{{$award->organization_name_award}}</td>
                                    <td class="col-sm-3">{{$award->pertinent_details}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Grants and scholarships</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-4">Grant or scholarship name</th>
                                <th class="col-sm-4">Date awarded</th>
                                <th class="col-sm-4">The institution that provided the award.</th>
                            </tr>
                            @foreach($grants as $grant)
                                <tr class="d-flex">
                                    <td class="col-sm-4">{{$grant->grant_name_scholarship}}</td>
                                    <td class="col-sm-4">{{$grant->date_awarded}}</td>
                                    <td class="col-sm-4">{{$grant->institute_award_provider}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <table class="table table-bordered">
                            <thead>
                            <tr class="d-flex text-lg-center">
                                <th class="col-12">Licenses and certifications</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="d-flex">
                                <th class="col-sm-4">License or certificate name</th>
                                <th class="col-sm-4">Date earned</th>
                                <th class="col-sm-4">Institution that awarded it.</th>
                            </tr>
                            @foreach($licenses as $license)
                                <tr class="d-flex">
                                    <td class="col-sm-4">{{$license->license_name}}</td>
                                    <td class="col-sm-4">{{$license->date_earned}}</td>
                                    <td class="col-sm-4">{{$license->awarded_institution}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
