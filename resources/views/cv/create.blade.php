@extends('layouts.app')

@section('content')
    <style>
        hr {
            height:1px;
            border:none;
            color:#333;
            background-color:#333;
        }
        .delete {
            height: 30px;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Your CV') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('cv.store')}}">
                            @csrf
                            <h3>Contact Info</h3>
                            <div class="container-fluid">
                                <div class="row">
                            <div class="form-group col-sm mr-1">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Name" name="name">
                            </div>
                            <div class="form-group col-sm mr-1">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" aria-describedby="emailHelp"
                                       placeholder="Enter email" name="email">
                            </div>
                            <div class="form-group col-sm mr-1">
                                <label for="telefon_number">Telefon Number</label>
                                <input type="number" class="form-control" id="telefon_number"
                                       placeholder="Telefon Number" name="telefon_number">
                            </div>
                            <div class="form-group col-sm mr-1">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" placeholder="Address"
                                       name="address">
                            </div>
                                </div>
                            </div>
                            <h3>Academic history</h3>
                                <div class="academic_history container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="school_name">School Name</label>
                                            <input type="text" class="form-control" id="school"
                                                   placeholder="School Name" name="school[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="title_of_degree">Title of the degree</label>
                                            <input type="text" class="form-control" id="title_of_degree"
                                                   placeholder="Title of the degree" name="title_of_degree[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="year_started">The year you started</label>
                                            <input type="number" class="form-control" id="year_started"
                                                   placeholder="The year you started" name="year_started[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="year_graduated">The year you graduated</label>
                                            <input type="number" class="form-control" id="year_graduated"
                                                   placeholder="The year you started" name="year_graduated[]">
                                        </div>
                                    </div>
                                </div>
                            <button type="button" class="btn btn-primary add_acedemic_button mb-2">+ Academic history</button>
                            <h3>Professional experience</h3>
                                <div class="professional_experience container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="organization_name">Name of the organization</label>
                                            <input type="text" class="form-control" id="organization_name"
                                                   placeholder="Name of the organization" name="organization_name[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="job_title">Job title</label>
                                            <input type="text" class="form-control" id="job_title"
                                                   placeholder="Job title" name="job_title[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="date_employed">Employed date</label>
                                            <input type="number" class="form-control" id="date_employed"
                                                   placeholder="Employed date" name="date_employed[]">
                                            <small>Start - End Date</small>
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="experience_achievements">Achievements.</label>
                                            <input type="text" class="form-control" id="experience_achievements"
                                                   placeholder="Achievements" name="experience_achievements[]">
                                        </div>
                                </div>
                                </div>
                            <button type="button" class="btn btn-primary add_professional_button mb-2">+ Professional experience
                            </button>
                            <h3>Qualifications and skills</h3>
                                <div class="qualifications_skills container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="hard_skills">Hard skills</label>
                                            <input type="text" class="form-control" id="hard_skills"
                                                   placeholder="Hard skills" name="hard_skills[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="soft_skills">Soft skills</label>
                                            <input type="text" class="form-control" id="soft_skills"
                                                   placeholder="Soft skills" name="soft_skills[]">
                                        </div>
                                    </div>
                                </div>
                            <button type="button" class="btn btn-primary add_qualification_button mb-2">+ Qualifications and skills
                            </button>
                            <h3>Awards and honors</h3>
                                <div class="awards_honors container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="award_name">Award name</label>
                                            <input type="text" class="form-control" id="award_name"
                                                   placeholder="Award name" name="award_name[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="year_received">Year received</label>
                                            <input type="text" class="form-control" id="year_received"
                                                   placeholder="Year received" name="year_received[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="organization_name_award">Organization name</label>
                                            <input type="text" class="form-control" id="organization_name_award"
                                                   placeholder="Organization name"
                                                   name="organization_name_award[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="pertinent_details">Pertinent details</label>
                                            <input type="text" class="form-control" id="pertinent_details"
                                                   placeholder="Pertinent details" name="pertinent_details[]">
                                        </div>
                                    </div>
                                </div>

                            <button type="button" class="btn btn-primary add_awards_button mb-2">+ Awards and honors
                            </button>

                            <h3>Grants and scholarships</h3>
                                <div class="grants_scholarships container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="grant_name_scholarship">Grant or Scholarship name</label>
                                            <input type="text" class="form-control" id="grant_name_scholarship"
                                                   placeholder="Grant or Scholarship name"
                                                   name="grant_name_scholarship[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="date_awarded">Date awarded</label>
                                            <input type="date" class="form-control" id="date_awarded"
                                                   placeholder="Date awarded" name="date_awarded[]">
                                        </div>

                                        <div class="form-group col-sm mr-1">
                                            <label for="institute_award_provider">Institution name.</label>
                                            <input type="text" class="form-control" id="institute_award_provider"
                                                   placeholder="Institution name."
                                                   name="institute_award_provider[]">
                                        </div>
                                    </div>
                                </div>
                            <button type="button" class="btn btn-primary add_grants_button mb-2">+ Grants and scholarships</button>
                            <h3>Licenses and certifications</h3>
                                <div class="licenses_certifications container-fluid">
                                    <div class="row">
                                        <div class="form-group col-sm mr-1">
                                            <label for="license_name">License or certificate name.</label>
                                            <input type="text" class="form-control" id="license_name"
                                                   placeholder="License or certificate name."
                                                   name="license_name[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="date_earned">The date you earned it.</label>
                                            <input type="date" class="form-control" id="date_earned"
                                                   placeholder="The date you earned it." name="date_earned[]">
                                        </div>
                                        <div class="form-group col-sm mr-1">
                                            <label for="awarded_institution">Institution that awarded it.</label>
                                            <input type="text" class="form-control" id="awarded_institution"
                                                   placeholder="Institution that awarded it.."
                                                   name="awarded_institution[]">
                                        </div>
                                    </div>
                            </div>
                            <button type="button" class="btn btn-primary add_certifications_button mb-2">+ Licenses and certifications</button>
                            <div>
                            <button type="submit" class="btn btn-success btn-lg">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            var max_fields_limit = 5; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_acedemic_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.academic_history').append(
                        '<div class="row remove_field_academic"><div class="form-group col-sm mr-1">' +
                        '<label for="school_name">School Name</label>' +
                        '<input type="text" class="form-control" id="school" placeholder="School Name" name="school[]">' +
                        '</div><div class="form-group col-sm mr-1">' +
                        '<label for="title_of_degree">Title of the degree</label>' +
                        '<input type="text" class="form-control" id="title_of_degree" placeholder="Title of the degree" name="title_of_degree[]">' +
                        '</div><div class="form-group col-sm mr-1">' +
                        '<label for="year_started">The year you started</label>\n' +
                        '<input type="number" class="form-control" id="year_started" placeholder="The year you started" name="year_started[]">' +
                        '</div><div class="form-group col-sm mr-1">\n' +
                        '<label for="year_graduated">The year you graduated</label>\n' +
                        '<input type="number" class="form-control" id="year_graduated" placeholder="The year you started" name="year_graduated[]">' +
                        '</div><a href="#" class="btn btn-danger remove_field_academic h-25 p-1">Delete</a></div>'); //add input field
                }
            });
            $('.academic_history').on("click", ".remove_field_academic", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document).ready(function () {
            var max_fields_limit = 5; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_professional_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.professional_experience').append(
                        '<div class="row">\n' +
                        '<div class="form-group col-sm mr-1" >\n' +
                        '<label for="organization_name">Name of the organization</label>\n' +
                        '<input type="text" class="form-control" id="organization_name" placeholder="Name of the organization" name="organization_name[]">\n' +
                        ' </div>' +
                        ' <div class="form-group col-sm mr-1">\n' +
                        '<label for="job_title">Job title</label>\n' +
                        '<input type="text" class="form-control" id="job_title" placeholder="Job title" name="job_title[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="date_employed">Employed date</label>\n' +
                        '                                    <input type="number" class="form-control" id="date_employed" placeholder="Employed date" name="date_employed[]">\n' +
                        '                                    <small>Start - End Date</small>\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="experience_achievements">Achievements.</label>\n' +
                        '                                    <input type="text" class="form-control" id="experience_achievements" placeholder="Achievements." name="experience_achievements[]">\n' +
                        '                                </div>\n' +
                        ' <a href="#" class="btn btn-danger remove_field_professional h-25 p-1">Delete</a></div>'
                    ); //add input field
                }
            });
            $('.professional_experience').on("click", ".remove_field_professional", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });

        $(document).ready(function () {
            var max_fields_limit = 10; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_qualification_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.qualifications_skills').append(
                        '<div class="row">\n' +
                        '<div class="form-group col-sm mr-1">\n' +
                        '<label for="hard_skills">Hard skills</label>\n' +
                        '                                    <input type="text" class="form-control" id="hard_skills" placeholder="Hard skills" name="hard_skills[]">\n' +
                        '                                </div>\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="soft_skills">Soft skills</label>\n' +
                        '<input type="text" class="form-control" id="soft_skills" placeholder="Soft skills" name="soft_skills[]">\n' +
                        '</div>\n' +
                        '<a href="#" class="btn btn-danger remove_field_qualification h-25 p-1">Delete</a></div>'); //add input field
                }
            });
            $('.qualifications_skills').on("click", ".remove_field_qualification", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
        $(document).ready(function () {
            var max_fields_limit = 10; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_awards_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.awards_honors').append(
                        '<div class="row">\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="award_name">Award name</label>\n' +
                        '                                    <input type="text" class="form-control" id="award_name" placeholder="Award name" name="award_name[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="year_received">Year received</label>\n' +
                        '                                    <input type="text" class="form-control" id="year_received" placeholder="Year received" name="year_received[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="organization_name_award">Organization name</label>\n' +
                        '                                    <input type="text" class="form-control" id="organization_name_award" placeholder="Organization name" name="organization_name_award[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="pertinent_details">Pertinent details</label>\n' +
                        '                                    <input type="text" class="form-control" id="pertinent_details" placeholder="Pertinent details" name="pertinent_details[]">\n' +
                        '</div>' +
                        '<a href="#" class="btn btn-sm delete btn-danger remove_field_awards h-25 p-1">Delete</a></div>'); //add input field
                }
            });
            $('.awards_honors').on("click", ".remove_field_awards", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
        $(document).ready(function () {
            var max_fields_limit = 10; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_grants_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.grants_scholarships').append(
                        '<div class="row">\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="grant_name_scholarship">Grant or Scholarship name</label>\n' +
                        '                                    <input type="text" class="form-control" id="grant_name_scholarship" placeholder="Grant or Scholarship name" name="grant_name_scholarship[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="date_awarded">Date awarded</label>\n' +
                        '                                    <input type="date" class="form-control" id="date_awarded" placeholder="Date awarded" name="date_awarded[]">\n' +
                        '                                </div>\n' +
                        '\n' +
                        '                                <div class="form-group col-sm mr-1">\n' +
                        '                                    <label for="institute_award_provider">Institution name.</label>\n' +
                        '                                    <input type="text" class="form-control" id="institute_award_provider" placeholder="Institution name." name="institute_award_provider[]">\n' +
                        '                                </div>\n' +
                        '<a href="#" class="btn btn-danger remove_field_grants h-25 p-1">Delete</a></div>'); //add input field
                }
            });
            $('.grants_scholarships').on("click", ".remove_field_grants", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
        $(document).ready(function () {
            var max_fields_limit = 10; //set limit for maximum input fields
            var x = 1; //initialize counter for text box
            $('.add_certifications_button').click(function (e) { //click event on add more fields button having class add_more_button
                e.preventDefault();
                if (x < max_fields_limit) { //check conditions
                    x++; //counter increment
                    $('.licenses_certifications').append(
                        '<div class="row">\n' +
                        '                                        <div class="form-group col-sm mr-1">\n' +
                        '                                            <label for="license_name">License or certificate name.</label>\n' +
                        '                                            <input type="text" class="form-control" id="license_name"\n' +
                        '                                                   placeholder="License or certificate name."\n' +
                        '                                                   name="license_name[]">\n' +
                        '                                        </div>\n' +
                        '                                        <div class="form-group col-sm mr-1">\n' +
                        '                                            <label for="date_earned">The date you earned it.</label>\n' +
                        '                                            <input type="date" class="form-control" id="date_earned"\n' +
                        '                                                   placeholder="The date you earned it." name="date_earned[]">\n' +
                        '                                        </div>\n' +
                        '                                        <div class="form-group col-sm mr-1">\n' +
                        '                                            <label for="awarded_institution">Institution that awarded it.</label>\n' +
                        '                                            <input type="text" class="form-control" id="awarded_institution"\n' +
                        '                                                   placeholder="Institution that awarded it.."\n' +
                        '                                                   name="awarded_institution[]">\n' +
                        '                                        </div>\n' +
                        '<a href="#" class="btn btn-danger remove_field_certifications float-right h-25 p-1">Delete</a></div>'); //add input field
                }
            });
            $('.licenses_certifications').on("click", ".remove_field_certifications", function (e) { //user click on remove text links
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })
        });
    </script>
@endsection
