@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">
                <label>{{ __('Welcome to CV Menagement System') }}</label>
                </div>
                <label>To see or create your CV please <a href="{{route('cv.index')}}">click here</a></label>
                </div>
        </div>
    </div>
</div>
@endsection
