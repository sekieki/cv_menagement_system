<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfessionalExperience extends Model
{
    protected $table = 'professional_experience';

    protected $fillable = ['organization_name', 'job_title', 'date_employed', 'title_of_degree','user_id'];
}
