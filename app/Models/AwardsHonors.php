<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AwardsHonors extends Model
{
    protected $table = 'awards_honors';

    protected $fillable = ['award_name', 'year_received', 'organization_name_award', 'pertinent_details','user_id'];
}
