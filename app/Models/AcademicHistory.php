<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicHistory extends Model
{
    protected $table = 'academic_history';

    protected $fillable = ['school', 'title_of_degree', 'year_started', 'year_graduated','user_id'];
}
