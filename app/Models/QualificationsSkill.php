<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QualificationsSkill extends Model
{
    protected $table = 'qualifications_skills';

    protected $fillable = ['hard_skills', 'soft_skills','user_id'];
}
