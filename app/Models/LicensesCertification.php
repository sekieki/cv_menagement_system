<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LicensesCertification extends Model
{
    protected $table = 'licenses_certification';

    protected $fillable = ['license_name', 'date_earned', 'awarded_institution','user_id'];
}
