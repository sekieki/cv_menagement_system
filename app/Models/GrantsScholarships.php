<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GrantsScholarships extends Model
{
    protected $table = 'grants_scholarships';

    protected $fillable = ['grant_name_scholarship', 'date_awarded', 'institute_award_provider','user_id'];
}
