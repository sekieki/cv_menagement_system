<?php

namespace App\Http\Controllers\CV;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AcademicHistory;
use App\Models\AwardsHonors;
use App\Models\ContactInfo;
use App\Models\GrantsScholarships;
use App\Models\LicensesCertification;
use App\Models\ProfessionalExperience;
use App\Models\QualificationsSkill;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter\AlignFormatter;

class CvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $contacts = ContactInfo::Where ('user_id',$user->id)->get();
        $academics = AcademicHistory::Where('user_id', $user->id)->get();
        $professionals = ProfessionalExperience::Where('user_id',$user->id)->get();
        $qualifications = QualificationsSkill::Where('user_id',$user->id)->get();
        $awards = AwardsHonors::Where('user_id',$user->id)->get();
        $grants = GrantsScholarships::Where('user_id',$user->id)->get();
        $licenses = LicensesCertification::Where('user_id',$user->id)->get();

        return view('cv.index')->with([
            'contacts'=> $contacts,
            'academics'=>$academics,
            'professionals'=>$professionals,
            'qualifications'=>$qualifications,
            'awards'=>$awards,
            'grants'=>$grants,
            'licenses'=>$licenses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        $contact_data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'telefon_number' => $request->input('telefon_number'),
            'address' => $request->input('address'),
            'user_id' => $user->id,
        ];

        $contact_info = ContactInfo::create($contact_data);

        $schoolname = $request->school;
        $title_of_degree = $request->title_of_degree;
        $year_started = $request->year_started;
        $year_graduated = $request->year_graduated;
        $user_id = $user->id;
        if (count($schoolname) > count($title_of_degree)) {
            $count = count($title_of_degree);
        } else {
            $count = count($schoolname);
        }
        for ($i = 0; $i < $count; $i++) {
            $academic_history = new AcademicHistory();
            $academic_history->school = $schoolname[$i];
            $academic_history->title_of_degree = $title_of_degree[$i];
            $academic_history->year_started = $year_started[$i];
            $academic_history->year_graduated = $year_graduated[$i];
            $academic_history->user_id = $user_id;
            $academic_history->save();
        }

        $organization_name = $request->organization_name;
        $job_title = $request->job_title;
        $date_employed = $request->date_employed;
        $experience_achievements = $request->experience_achievements;
        $user_id = $user->id;
        if (count($organization_name) > count($job_title)) {
            $count = count($job_title);
        } else {
            $count = count($organization_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $professional_experience = new ProfessionalExperience();
            $professional_experience->organization_name = $organization_name[$i];
            $professional_experience->job_title = $job_title[$i];
            $professional_experience->date_employed = $date_employed[$i];
            $professional_experience->experience_achievements = $experience_achievements[$i];
            $professional_experience->user_id = $user_id;
            $professional_experience->save();
        }

        $hard_skills = $request->hard_skills;
        $soft_skills = $request->soft_skills;
        $user_id = $user->id;
        if (count($hard_skills) > count($soft_skills)) {
            $count = count($soft_skills);
        } else {
            $count = count($hard_skills);
        }
        for ($i = 0; $i < $count; $i++) {
            $qualifications_skills = new QualificationsSkill();
            $qualifications_skills->hard_skills = $hard_skills[$i];
            $qualifications_skills->soft_skills = $soft_skills[$i];
            $qualifications_skills->user_id = $user_id;
            $qualifications_skills->save();
        }

        $award_name = $request->award_name;
        $year_received = $request->year_received;
        $organization_name_award = $request->organization_name_award;
        $pertinent_details = $request->pertinent_details;
        $user_id = $user->id;
        if (count($award_name) > count($year_received)) {
            $count = count($year_received);
        } else {
            $count = count($award_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $awards_honors = new AwardsHonors();
            $awards_honors->award_name = $award_name[$i];
            $awards_honors->year_received = $year_received[$i];
            $awards_honors->organization_name_award = $organization_name_award[$i];
            $awards_honors->pertinent_details = $pertinent_details[$i];
            $awards_honors->user_id = $user_id;
            $awards_honors->save();
        }

        $grant_name_scholarship = $request->grant_name_scholarship;
        $date_awarded = $request->date_awarded;
        $institute_award_provider = $request->institute_award_provider;
        $user_id = $user->id;
        if (count($grant_name_scholarship) > count($date_awarded)) {
            $count = count($date_awarded);
        } else {
            $count = count($grant_name_scholarship);
        }
        for ($i = 0; $i < $count; $i++) {
            $grants_scholarships = new GrantsScholarships();
            $grants_scholarships->grant_name_scholarship = $grant_name_scholarship[$i];
            $grants_scholarships->date_awarded = $date_awarded[$i];
            $grants_scholarships->institute_award_provider = $institute_award_provider[$i];
            $grants_scholarships->user_id = $user_id;
            $grants_scholarships->save();
        }

        $license_name = $request->license_name;
        $date_earned = $request->date_earned;
        $awarded_institution = $request->awarded_institution;
        $user_id = $user->id;
        if (count($license_name) > count($date_earned)) {
            $count = count($date_earned);
        } else {
            $count = count($license_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $licenses_certification  = new LicensesCertification();
            $licenses_certification->license_name = $license_name[$i];
            $licenses_certification->date_earned = $date_earned[$i];
            $licenses_certification->awarded_institution = $awarded_institution[$i];
            $licenses_certification->user_id = $user_id;
            $licenses_certification->save();
        }

        return redirect()->route('cv.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();
        $contact = ContactInfo::Where ('user_id',$user->id)->get();
        $academic = AcademicHistory::Where('user_id', $user->id)->get();
        $professional = ProfessionalExperience::Where('user_id',$user->id)->get();
        $qualifications = QualificationsSkill::Where('user_id',$user->id)->get();
        $awards = AwardsHonors::Where('user_id',$user->id)->get();
        $grants = GrantsScholarships::Where('user_id',$user->id)->get();
        $licenses = LicensesCertification::Where('user_id',$user->id)->get();
        return view('cv.edit')->with([
            'contact'=>$contact,
            'academic'=>$academic,
            'professional'=>$professional,
            'qualifications'=>$qualifications,
            'awards'=>$awards,
            'grants'=>$grants,
            'licenses'=>$licenses,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $contact_info=ContactInfo::where('user_id',$user_id)->first();
        $contact_info->name=$request->input('name');
        $contact_info->email=$request->input('email');
        $contact_info->telefon_number=$request->input('telefon_number');
        $contact_info->address=$request->input('address');
        $contact_info->user_id=$user_id;
        $contact_info->save();

        $schoolname = $request->school;
        $title_of_degree = $request->title_of_degree;
        $year_started = $request->year_started;
        $year_graduated = $request->year_graduated;
        $user_id = $user->id;
        if (count($schoolname) > count($title_of_degree)) {
            $count = count($title_of_degree);
        } else {
            $count = count($schoolname);
        }
        for ($i = 0; $i < $count; $i++) {
            $academic_history = AcademicHistory::where('user_id',$user_id)->first();
            $academic_history->school = $schoolname[$i];
            $academic_history->title_of_degree = $title_of_degree[$i];
            $academic_history->year_started = $year_started[$i];
            $academic_history->year_graduated = $year_graduated[$i];
            $academic_history->user_id = $user_id;
            $academic_history->save();
        }

        $organization_name = $request->organization_name;
        $job_title = $request->job_title;
        $date_employed = $request->date_employed;
        $experience_achievements = $request->experience_achievements;
        $user_id = $user->id;
        if (count($organization_name) > count($job_title)) {
            $count = count($job_title);
        } else {
            $count = count($organization_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $professional_experience = ProfessionalExperience::where('user_id',$user_id)->first();
            $professional_experience->organization_name = $organization_name[$i];
            $professional_experience->job_title = $job_title[$i];
            $professional_experience->date_employed = $date_employed[$i];
            $professional_experience->experience_achievements = $experience_achievements[$i];
            $professional_experience->user_id = $user_id;
            $professional_experience->save();
        }

        $hard_skills = $request->hard_skills;
        $soft_skills = $request->soft_skills;
        $user_id = $user->id;
        if (count($hard_skills) > count($soft_skills)) {
            $count = count($soft_skills);
        } else {
            $count = count($hard_skills);
        }
        for ($i = 0; $i < $count; $i++) {
            $qualifications_skills = QualificationsSkill::where('user_id',$user_id)->first();
            $qualifications_skills->hard_skills = $hard_skills[$i];
            $qualifications_skills->soft_skills = $soft_skills[$i];
            $qualifications_skills->user_id = $user_id;
            $qualifications_skills->save();
        }

        $award_name = $request->award_name;
        $year_received = $request->year_received;
        $organization_name_award = $request->organization_name_award;
        $pertinent_details = $request->pertinent_details;
        $user_id = $user->id;
        if (count($award_name) > count($year_received)) {
            $count = count($year_received);
        } else {
            $count = count($award_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $awards_honors = AwardsHonors::where('user_id',$user_id)->first();
            $awards_honors->award_name = $award_name[$i];
            $awards_honors->year_received = $year_received[$i];
            $awards_honors->organization_name_award = $organization_name_award[$i];
            $awards_honors->pertinent_details = $pertinent_details[$i];
            $awards_honors->user_id = $user_id;
            $awards_honors->save();
        }

        $grant_name_scholarship = $request->grant_name_scholarship;
        $date_awarded = $request->date_awarded;
        $institute_award_provider = $request->institute_award_provider;
        $user_id = $user->id;
        if (count($grant_name_scholarship) > count($date_awarded)) {
            $count = count($date_awarded);
        } else {
            $count = count($grant_name_scholarship);
        }
        for ($i = 0; $i < $count; $i++) {
            $grants_scholarships = GrantsScholarships::where('user_id',$user_id)->first();;
            $grants_scholarships->grant_name_scholarship = $grant_name_scholarship[$i];
            $grants_scholarships->date_awarded = $date_awarded[$i];
            $grants_scholarships->institute_award_provider = $institute_award_provider[$i];
            $grants_scholarships->user_id = $user_id;
            $grants_scholarships->save();
        }

        $license_name = $request->license_name;
        $date_earned = $request->date_earned;
        $awarded_institution = $request->awarded_institution;
        $user_id = $user->id;
        if (count($license_name) > count($date_earned)) {
            $count = count($date_earned);
        } else {
            $count = count($license_name);
        }
        for ($i = 0; $i < $count; $i++) {
            $licenses_certification = LicensesCertification::where('user_id',$user_id)->first();;
            $licenses_certification->license_name = $license_name[$i];
            $licenses_certification->date_earned = $date_earned[$i];
            $licenses_certification->awarded_institution = $awarded_institution[$i];
            $licenses_certification->user_id = $user_id;
            $licenses_certification->save();
        }

        return redirect()->route('cv.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
