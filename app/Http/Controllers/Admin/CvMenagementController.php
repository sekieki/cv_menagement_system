<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AcademicHistory;
use App\Models\AwardsHonors;
use App\Models\ContactInfo;
use App\Models\GrantsScholarships;
use App\Models\LicensesCertification;
use App\Models\ProfessionalExperience;
use App\Models\QualificationsSkill;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CvMenagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.cvmenagement.index')->with('users',User::paginate(10));
    }

    public function cvblade($id)
    {
        if (Auth::user()->id==$id){
            return redirect()->route('admin.cvmenagement.index')->with('warning','If you want to see your CV go to View your CV');
        }

        $user = User::find ($id);
        $contacts = ContactInfo::Where ('user_id',$user->id)->get();
        $academics = AcademicHistory::Where('user_id', $user->id)->get();
        $professionals = ProfessionalExperience::Where('user_id',$user->id)->get();
        $qualifications = QualificationsSkill::Where('user_id',$user->id)->get();
        $awards = AwardsHonors::Where('user_id',$user->id)->get();
        $grants = GrantsScholarships::Where('user_id',$user->id)->get();
        $licenses = LicensesCertification::Where('user_id',$user->id)->get();

        return view('admin.cvmenagement.cvblade')->with([
            'contacts'=> $contacts,
            'academics'=>$academics,
            'professionals'=>$professionals,
            'qualifications'=>$qualifications,
            'awards'=>$awards,
            'grants'=>$grants,
            'licenses'=>$licenses,
            'user'=>User::find($id)
        ]);
    }
}
